import 'package:flutter/material.dart';

class customWidgets extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return Container(
      color: Colors.pink,
      child: new Container(
        color: Colors.orange,
        margin: const EdgeInsets.all(15.0),
        child: new Container(
          color: Colors.red,
          margin: const EdgeInsets.all(15.0),
          child: new Container(
            color: Colors.green,
            margin: const EdgeInsets.all(15.0),
            child: new Container(
              color: Colors.blue,
              margin: const EdgeInsets.all(15.0),
              child: new Container(
                color: Colors.black,
                margin: const EdgeInsets.all(15.0),
              ),
            ),
          ),
        ),
      ),
    );
  }
}
